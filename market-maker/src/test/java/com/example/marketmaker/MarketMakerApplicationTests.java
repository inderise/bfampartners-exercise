package com.example.marketmaker;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.integration.dsl.IntegrationFlow;
import org.springframework.integration.dsl.IntegrationFlows;
import org.springframework.integration.dsl.MessageChannels;
import org.springframework.integration.dsl.context.IntegrationFlowContext;
import org.springframework.integration.ip.dsl.Tcp;
import org.springframework.integration.ip.tcp.serializer.TcpCodecs;

@SpringBootTest(classes = { MarketMakerApplication.class} )
class MarketMakerApplicationTests {

	@Autowired
	IntegrationFlowContext flowContext;

	@Autowired
	ApplicationContext context;

	@Test
	public void whenRequestFormattingIsInCorrect() {
		IntegrationFlow client = IntegrationFlows.from(MessageChannels.direct())
				.handle(Tcp.outboundGateway(
						Tcp.netClient("localhost", 1234)
								.serializer(TcpCodecs.crlf())
								.deserializer(TcpCodecs.crlf())
								.connectTimeout(2)
								.singleUseConnections(true))
						.closeStreamAfterSend(true))
				.<byte[], String>transform(b -> new String(b)).get();
		IntegrationFlowContext.IntegrationFlowRegistration registration = flowContext.registration(client).id("client").register();
		String received = registration.getMessagingTemplate().convertSendAndReceive("123 BOY", String.class);
		Assertions.assertThat(received).isEqualTo("BAD REQUEST: Usage is {SecurityId} {BUY|SELL} {Quantity}");
		flowContext.remove("client");
	}

	@Test
	public void withCorrectRequest() {
		IntegrationFlow client = IntegrationFlows.from(MessageChannels.direct())
				.handle(Tcp.outboundGateway(
						Tcp.netClient("localhost", 1234)
								.serializer(TcpCodecs.crlf())
								.deserializer(TcpCodecs.crlf())
								.connectTimeout(2)
								.singleUseConnections(true))
						.closeStreamAfterSend(true))
				.<byte[], String>transform(b -> new String(b)).get();
		IntegrationFlowContext.IntegrationFlowRegistration registration = flowContext.registration(client).id("client").register();
		String received = registration.getMessagingTemplate().convertSendAndReceive("123 BUY 300", String.class);
		Assertions.assertThat(Double.valueOf(received)).isNotNaN();
		flowContext.remove("client");
	}


}
