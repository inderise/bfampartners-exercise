package com.example.marketmaker.handler;

import com.example.marketmaker.QuoteRequest;
import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Test;

public class RequestValidateTest {

    private final RequestValidator validator = new RequestValidator();

    @Test(expected = IllegalArgumentException.class)
    public void whenRequestWithInvalidFormat() {
        validator.validate("123 BUY");
    }

    @Test(expected = NumberFormatException.class)
    public void whenSecurityIdIsNotInteger() {
        validator.validate("XXX BUY 100");
    }

    @Test(expected = IllegalArgumentException.class)
    public void whenBuySellIndicatorIsWrong() {
        validator.validate("123 BOY 50");
    }

    @Test(expected = IllegalArgumentException.class)
    public void whenBuySellIndicatorSmallCase() {
        validator.validate("123 sell 75");
    }

    @Test(expected = NumberFormatException.class)
    public void whenQuantityIsNotInteger() {
        validator.validate("123 SELL XXX");
    }

    @Test(expected = IllegalArgumentException.class)
    public void whenSecurityIsNegative() {
        validator.validate("-111 BUY 100");
    }

    @Test(expected = IllegalArgumentException.class)
    public void whenQuantityIsNegative() {
        validator.validate("123 BUY -100");
    }

    @Test
    public void whenRequestHasRightFormat() {
        QuoteRequest request = validator.validate("123 BUY 100");
        Assert.assertThat(request, Matchers.samePropertyValuesAs(new QuoteRequest(123, true, 100)));
    }

}