package com.example.marketmaker;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
@AllArgsConstructor
public class QuoteRequest {

    private int securityId;
    private boolean buy;
    private int quantity;
}
