package com.example.marketmaker.quoting;

import com.example.marketmaker.QuoteCalculationEngine;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Component;

import java.util.Random;

@Log4j2
@Component
public class QuoteCalculationEngineImpl implements QuoteCalculationEngine {
    @Override
    public double calculateQuotePrice(int securityId, double referencePrice, boolean buy, int quantity) {
        double quote = new Random().nextDouble();
        log.info("Quote price determined is {}", quote);
        return quote;
    }
}
