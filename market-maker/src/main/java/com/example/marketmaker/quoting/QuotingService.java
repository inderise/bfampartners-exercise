package com.example.marketmaker.quoting;

import com.example.marketmaker.QuoteCalculationEngine;
import com.example.marketmaker.ReferencePriceSource;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class QuotingService {

    private final ReferencePriceSource referencePriceSource;
    private final QuoteCalculationEngine quoteCalculationEngine;

    public double getPrice(int securityId, boolean buy, int quantity) {
        double reference = referencePriceSource.get(securityId);
        return quoteCalculationEngine.calculateQuotePrice(securityId, reference, buy, quantity);
    }

}
