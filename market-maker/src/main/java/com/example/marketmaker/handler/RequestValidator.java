package com.example.marketmaker.handler;

import com.example.marketmaker.QuoteRequest;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;
import org.springframework.util.NumberUtils;

@Component
public class RequestValidator {

    public QuoteRequest validate(String request) {
        String[] tokens = request.split(" ");
        Assert.isTrue(tokens.length == 3, "Invalid request format");
        int securityId = NumberUtils.parseNumber(tokens[0], Integer.class);
        Assert.isTrue(securityId > 0, "SecurityId cannot be negative");
        boolean test = "BUY".equals(tokens[1]) || "SELL".equals(tokens[1]);
        Assert.isTrue(test, "Invalid request format");
        int quantity = NumberUtils.parseNumber(tokens[2], Integer.class);
        Assert.isTrue(quantity > 0, "Quantity cannot be negative");
        return new QuoteRequest(securityId, "BUY".equals(tokens[1]), quantity);
    }

}
