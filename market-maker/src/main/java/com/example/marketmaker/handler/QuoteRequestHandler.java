package com.example.marketmaker.handler;

import com.example.marketmaker.QuoteRequest;
import com.example.marketmaker.quoting.QuotingService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
@RequiredArgsConstructor
public class QuoteRequestHandler {

    private final RequestValidator validator;
    private final QuotingService quotingService;

    public String handle(String request) {
        Optional<QuoteRequest> quoteRequest = parseRequest(request);
        if (quoteRequest.isEmpty()) {
            return "BAD REQUEST: Usage is {SecurityId} {BUY|SELL} {Quantity}";
        } else {
            QuoteRequest req = quoteRequest.get();
            return String.valueOf(quotingService.getPrice(req.getSecurityId(), req.isBuy(), req.getQuantity()));
        }
    }

    private Optional<QuoteRequest> parseRequest(String request) {
        try{
            QuoteRequest quoteRequest = validator.validate(request);
            return Optional.of(quoteRequest);
        }catch (IllegalArgumentException ex) {
            return Optional.empty();
        }
    }
}
