package com.example.marketmaker.pricing;

import com.example.marketmaker.ReferencePriceSourceListener;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Log4j2
@Component
public class InMemoryReferencePriceStore implements ReferencePriceSourceListener {

    private final Map<Integer, Double> store = new ConcurrentHashMap<>();
    @Override
    public void referencePriceChanged(int securityId, double price) {
        store.put(securityId, price);
        log.info("reference price updated {} -> {}", securityId, price);
    }

    public double get(int securityId) {
        return store.containsKey(securityId) ? store.get(securityId) : 0.0;
    }
}
