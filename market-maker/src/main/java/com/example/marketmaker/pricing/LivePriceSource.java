package com.example.marketmaker.pricing;

import com.example.marketmaker.ReferencePriceSource;
import com.example.marketmaker.ReferencePriceSourceListener;
import com.example.marketmaker.SecurityReferencePrice;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;

import java.time.Duration;
import java.util.Random;

@Component
@RequiredArgsConstructor
public class LivePriceSource implements ReferencePriceSource {

    private final InMemoryReferencePriceStore store;
    @Override
    public void subscribe(ReferencePriceSourceListener listener) {
        referencePriceFlux().subscribe(x -> listener.referencePriceChanged(x.getSecurityId(), x.getPrice()));
    }

    @Override
    public double get(int securityId) {

        return store.get(securityId);
    }

    Flux<SecurityReferencePrice> referencePriceFlux () {
        int[] securityIds =  {11, 22, 33, 44, 55, 66, 77, 88, 99};
        return Flux.interval(Duration.ofSeconds(1))
                .map(x -> securityIds[new Random().nextInt(8)])
                .map(id -> new SecurityReferencePrice(id, new Random().nextDouble()));
    }
}
