package com.example.marketmaker;

import com.example.marketmaker.handler.QuoteRequestHandler;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.Bean;
import org.springframework.integration.dsl.IntegrationFlow;
import org.springframework.integration.dsl.IntegrationFlows;
import org.springframework.integration.ip.dsl.Tcp;
import org.springframework.integration.ip.tcp.serializer.TcpCodecs;

@Log4j2
@SpringBootApplication
public class MarketMakerApplication {

    public static void main(String[] args) {
        SpringApplication.run(MarketMakerApplication.class, args);
    }



    @Bean
    IntegrationFlow server(@Value("${server.tcp.port:1234}") int port, QuoteRequestHandler requestHandler) {
        return IntegrationFlows.from(
                Tcp.inboundGateway(Tcp.netServer(port)
                        .serializer(TcpCodecs.crlf())
                        .deserializer(TcpCodecs.crlf()))
        )
                .log()
                .<byte[], String>transform(b -> requestHandler.handle(new String(b)))
                .get();
    }

    @Bean
    ApplicationListener<ApplicationReadyEvent> ready(ReferencePriceSource referencePriceSource, ReferencePriceSourceListener listener) {
        return e -> {
            log.info("server is ready...");
            referencePriceSource.subscribe(listener);
        };
    }

}
