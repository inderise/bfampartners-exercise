package com.example.marketmaker;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
@AllArgsConstructor
public class SecurityReferencePrice {

    private int securityId;
    private double price;
}
