1. In order to run the project execute the command mvn spring-boot:run
2. The TCP server has been hosted using the spring integration inbound gateway approach using the java DSL.
3. The TCP server listens on configurable port (default is 1234) for quote requests.
4. Upon receipt of a message, the server transforms the message into a quote request.
5. If the msg validation fails an appropriate error message is returned to the client highlighting the request format.
6. Upon successfull msg validation, the server requests the quoting service to fetch a quote.
7. LivePriceSource implements the ReferencePriceSource interface and tries to mimic a realtime source of reference prices.
8. When a new reference price is discovered realtime, the same is passed on to the listener.
9. InMemory reference price store implements the ReferencePriceSourceListener.
10. The QuoteCalculationEngineImpl merely sends a random quote but one can imagine it would be a function of the inputs.
11. Finally the quote is returned to the client.
